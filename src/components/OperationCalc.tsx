import React from 'react'

type OperationButtons = {
    onclick: (e:any) => void
}

const numericOperations = [ '+' , '-', '*', '/' ]

const  OperationCalc :React.FunctionComponent <OperationButtons> = (props) => {
    
    const handleClick = (e:any) => {
        props.onclick(e.target.value)
    }

    return (
        <>
            {
                numericOperations.map( (item:any) => (
                    <div className = "buttonCal">
                        <button
                            className = {item} 
                            type      = "button"
                            onClick   = { handleClick }
                            key       = { item } 
                            value     = { item } 
                        >
                            { item }
                        </button>
                    </div>
                ))
            }
        </>
    )
}

export default OperationCalc