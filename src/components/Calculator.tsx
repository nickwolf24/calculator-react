import React, { useState, useEffect } from 'react'
import ButtonCalc  from './ButtonCalc' 
import OperationCalc from './OperationCalc'

type InitialValue = {
    initialValue : number
}
const  Calculator: React.FunctionComponent<InitialValue> = (props) => {

    const [clickedNumber, setClickedNumber] = useState<number[]>([props.initialValue])
    const [equal, setEqual]                 = useState<boolean>(false)
    const [result, setResult]               = useState<number | string>(0)

    const handleNumber = (numb:any) => {
        
        if (clickedNumber[0] == 0) {
            clickedNumber.shift();
        }
        
        if (numb =='AC') {
            console.log('estoy en AC')
            setClickedNumber([0])
        } else {
            setClickedNumber([...clickedNumber, numb])
        }
        
    }

    const handleEqualEvent = (event: any) => {
        setEqual(true)
    } 

    const handleFunction = (funcCalc: any) => {
        
        if (funcCalc == "=") {
            setEqual(true)
        } else {
            setClickedNumber([...clickedNumber, funcCalc])
            
        }
    }

    useEffect( ()=> {
        if (equal == true) {
            
            let values = ""
            
            clickedNumber.map((item)=> (
                values += item
            ))
                
            let pre_result = eval(`${ values.toString() }`)

            if (!pre_result) {
                setResult('!Error En la Operación')
            }
            
            setResult(pre_result)
            
            setClickedNumber([pre_result]);
            
            setEqual(false)
        }
    }, [equal])



    return (
        <div >
            <div className="title">
                Calculator
            </div>

            <br/>
            <br/>
            
            <div className="contentCal">
                {/* Visualizer */}
                <div className="visualizer">
                    { clickedNumber.length > 0 ? clickedNumber : result  }
                </div>
                {/* Operation Buttons */}
                <div className="buttonsContainer">
                    <div>
                        <OperationCalc onclick={ handleFunction } key='operationCalc' />
                    </div>
                    {/* Numbers Buttons */}
                    <div className= "contentNumeric">
                        <ButtonCalc onclick={ handleNumber }  key='numberCalc'/> 
                    </div>
                    <div className="containerEqual">
                        <span onClick={ handleEqualEvent } className="btnEqual" >=</span>
                        {/* <button onClick={ handleEqualEvent } className="btnEqual" value="=">=</button> */}
                    </div>
                </div>

            </div>
        
        </div>
    )
}

export default Calculator