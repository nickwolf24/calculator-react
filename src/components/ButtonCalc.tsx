import React from 'react'

type Button = {
    onclick: (e: any) => void
}

const tecladoNumerico = [ 7,8,9,4,5,6,1,2,3,0,".","AC" ]

const  buttonCalc: React.FunctionComponent<Button> = (props) => {
    
    const handleClick = (e: any) => {
        props.onclick(e.target.value)
    }

    return (
        <div className="buttonNumericContent">
            {
                tecladoNumerico.map( (item:any) => (
                    <div className = "buttonCal">
                        
                        <button
                            type    = "button"
                            className    = { item }
                            onClick = { handleClick }
                            key     = { item } 
                            value   = { item } 
                        > 
                            { item }
                        
                        </button>
                    </div>
                ))
            }

        </div>
    )
}

export default buttonCalc